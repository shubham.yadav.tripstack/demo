﻿using System;
using System.Collections.Generic;
using System.Text;

namespace flightlib
{
    public class flighcolors
    {
        private int _id;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("Id : {0}\tName : {1}",Id,Name);
        }

        //public void PrintColor()
        //{
        //    Console.WriteLine("ID : " + Id + "\tName : " + Name);
        //}
    }
}
