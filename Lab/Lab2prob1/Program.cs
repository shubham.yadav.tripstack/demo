﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2prob1
{
    public class Date
    {
        public string mDay { get; set; }
        public string mMonth { get; set; }
        public string mYear { get; set; }

        public Date()
        {
            mDay = "01"; mMonth = "02";mYear = "1996";
        }
        public void PrintDate()
        {
            Console.WriteLine(mDay+"-"+mMonth+"-"+mYear);
        }
        //public override string ToString()
        //{
        //    return string.Format(mDay+"-"+mMonth+"-"+mYear);
        //}


        static void Main(string[] args)
        {


            Date dataObj1 = new Date { mDay=(args[0]), mMonth=(args[1]), mYear=(args[2])};
            dataObj1.PrintDate();
            //Console.WriteLine(dataObj1);
         
            Date dataObj2 = new Date();
            dataObj2.PrintDate();
        }
        
    }
}
